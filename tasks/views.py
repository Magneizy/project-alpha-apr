from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView
from .models import Task
from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.


class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    template_name = "tasks/create.html"
    fields = ["name", "start_date", "due_date", "project", "assignee"]

    def get_success_url(self):
        return reverse_lazy("show_project", args=[self.object.id])


class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "tasks/list.html"

    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)


class TaskUpdateView(UpdateView):
    model = Task
    fields = ["is_completed"]
    template_name = "tasks/list.html"
    success_url = reverse_lazy("show_my_tasks")
